﻿using Accord.Video.FFMPEG;
using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Capture
{
    class Program
    {
        static void Main(string[] args)
        {
            FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            if (videoDevices.Count == 0)
            {
                throw new Exception();
            }

            //var l = new List<FilterInfo>(videoDevices.GetEnumerator() as IEnumerable<FilterInfo>);
            //var v = l.First(info => info.Name == "USB Video Device");

            //FilterInfo videoInfo = null;

            //foreach (FilterInfo info in videoDevices)
            //{
            //    if (info.Name == "USB Video Device") // My web cam
            //    {
            //        videoInfo = info;
            //        break;
            //    }
            //}

            //if (videoInfo == null) return;

            VideoCaptureDevice videoSource1 = new VideoCaptureDevice(videoDevices[0].MonikerString);

            var maxWidth = videoSource1.VideoCapabilities.Max(c => c.FrameSize.Width);
            videoSource1.VideoResolution = videoSource1.VideoCapabilities.First(c => c.FrameSize.Width == maxWidth);
        
            VideoFileWriter writer = new VideoFileWriter();

            int framesPerSecond = 15;
            int bitRate = ((videoSource1.VideoResolution.FrameSize.Width *
                videoSource1.VideoResolution.FrameSize.Height) * framesPerSecond);

            // create new video file
            writer.Open("test.avi",
                1920,
                1080,
                framesPerSecond,
                VideoCodec.H263P, // This looks the be the best all around that works.
                bitRate);

            var i = 0;
            var s = Stopwatch.StartNew();

            videoSource1.NewFrame += (object sender, NewFrameEventArgs eventArgs) =>
            {
                i++;
                s.Reset();
                
                //Bitmap clone = eventArgs.Frame.Clone(
                //    new Rectangle(0, 0, eventArgs.Frame.Width, eventArgs.Frame.Height), 
                //    PixelFormat.Format24bppRgb);

                writer.WriteVideoFrame(eventArgs.Frame);
                Console.WriteLine(s.ElapsedMilliseconds);
                if (i == 100)
                {
                    videoSource1.Stop();
                    writer.Close();
                }
            };

            videoSource1.Start();
        }
    }
}
