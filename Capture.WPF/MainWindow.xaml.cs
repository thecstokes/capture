﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Accord.Video.FFMPEG;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;
using System.Diagnostics;

namespace Capture.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VideoCaptureDevice webCam;
        public FilterInfoCollection webCamCollection;
        VideoFileWriter writer;


        public MainWindow()
        {
            InitializeComponent();
            Loaded += OnLoadEvent;
            Closed += OnCloseEvent;
        }



        private void OnLoadEvent(object sender, RoutedEventArgs e)
        {
            webCamCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            webCam = new VideoCaptureDevice(webCamCollection[0].MonikerString);
            webCam.NewFrame += FrameHandler;

            //Video Writer
            writer = new VideoFileWriter();

            int framesPerSecond = 15;
            int bitRate = ((1920 *
                1080) * framesPerSecond);

            // create new video file
            writer.Open("test.avi",
                1920,
                1080,
                framesPerSecond,
                VideoCodec.H263P, // This looks the be the best all around that works.
                bitRate);
            
            webCam.Start();


        }

        private void OnCloseEvent(object sender, EventArgs e)
        {
            webCam.NewFrame -= FrameHandler;
            Dispatcher.InvokeShutdown();
            webCam.SignalToStop();

            writer.Close();
        }

        private object _CameraImageLock = new object();
        
        int processed = 0, requested = 0;


        Stopwatch sw = new Stopwatch();

        private void FrameHandler(object sender, NewFrameEventArgs eventArgs)
        {
            //Capture
            try
            {
                MemoryStream ms = new MemoryStream();
                BitmapImage bi = new BitmapImage
                {
                    CacheOption = BitmapCacheOption.OnLoad  //Optimization for caching bmp
                };
                lock (_CameraImageLock)
                {
                    sw.Restart();
                    //img = (Bitmap)eventArgs.Frame.Clone();
                    eventArgs.Frame.Save(ms, ImageFormat.Bmp);
                    requested++;
                }
                ms.Seek(0, SeekOrigin.Begin);
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.EndInit();

                bi.Freeze();
                Application.Current.Dispatcher.BeginInvoke(new ThreadStart(delegate
                {
                    if (bi != null)
                        lock (_CameraImageLock)
                        {
                            Capture.Source = bi;
                            processed++;
                        }
                }));


                writer.WriteVideoFrame(eventArgs.Frame);
                eventArgs.Frame.Dispose(); // Free memory

                Console.WriteLine($"TOT TIME: {sw.ElapsedMilliseconds}");

            }
            catch (Exception ex)
            {
            }
        }
    }
}
